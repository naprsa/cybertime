from django.urls import path

from . import views

app_name = 'rooms'

urlpatterns = [
    path('login/<str:room_name>/', views.login, name='login'),
    path('room/<str:room_name>/', views.room, name='room'),
]
