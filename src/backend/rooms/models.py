from django.db import models
from django.shortcuts import reverse
from shortuuidfield import ShortUUIDField
from django.utils import timezone


# Create your models here.


class StudentGroup(models.Model):
    GROUP_STARTED = 'ST'
    GROUP_FINISHED = 'FI'
    STATUSES = (
        (GROUP_STARTED, 'Начали работать'),
        (GROUP_FINISHED, 'Закончили работать')
    )
    uid = ShortUUIDField(primary_key=True, max_length=10, verbose_name='Номер группы', editable=False)
    job_status = models.CharField(verbose_name='Статус выполнения', max_length=2,
                                  choices=STATUSES, blank=True, default='')
    points = models.IntegerField(verbose_name='Баллы', default=0)
    started = models.DateTimeField(verbose_name='Начали выполнять', editable=False, null=True, blank=True)
    finished = models.DateTimeField(verbose_name='Закончили выполнять', editable=False, null=True, blank=True)
    activity = models.ForeignKey('activity.Activity', verbose_name='Активность', related_name='students_groups',
                                 on_delete=models.CASCADE)
    additional_exercises = models.ManyToManyField('activity.Exercise', verbose_name='Доп. задания', blank=True)

    class Meta:
        ordering = ['activity']

    def __str__(self):
        return f'{self.activity.name}, {self.uid}'

    def get_absolute_url(self):
        return reverse('rooms:login', kwargs={'room_name': self.uid})

    def set_group_start(self):
        self.job_status = self.GROUP_STARTED
        self.started = timezone.now()
        self.save(update_fields=['job_status', 'started'])
        return self

    def set_group_finish(self):
        self.job_status = self.GROUP_FINISHED
        self.finished = timezone.now()
        self.save(update_fields=['job_status', 'finished'])
        return self


class Student(models.Model):
    uid = ShortUUIDField(primary_key=True, max_length=10, verbose_name='uid', editable=False)
    name = models.CharField(verbose_name='Имя студента', max_length=100)
    group = models.ForeignKey(StudentGroup, verbose_name='Группа', related_name='students', on_delete=models.CASCADE)
    ready = models.BooleanField(verbose_name='Готов', default=False, editable=False)
    online = models.BooleanField(verbose_name='Статус онлайн', default=True, editable=False)

    def __str__(self):
        return self.name

    def set_online(self, status: bool):
        self.online = status
        return self.save(update_fields=['online', ])

    def set_ready(self, status: bool):
        self.ready = status
        return self.save(update_fields=['ready', ])
