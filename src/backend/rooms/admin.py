from django.contrib import admin
from .models import StudentGroup, Student

admin.site.register(Student)
admin.site.register(StudentGroup)

# Register your models here.
