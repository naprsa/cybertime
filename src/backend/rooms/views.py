from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import StudentGroup, Student
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync


def login(request, room_name):
    if request.user.is_authenticated:
        return redirect('rooms:room', room_name)

    if request.session.get('uid'):
        user = Student.objects.get(pk=request.session.get('uid'))

        return redirect('rooms:room', user.group_id)

    if request.method == 'GET':
        return render(request, 'index.html', {'group_name': room_name})

    elif request.method == 'POST':
        name = request.POST.get('username')
        student, _ = Student.objects.get_or_create(name=name, group_id=room_name)
        request.session['uid'] = student.pk
        student.set_online(True)
        student.save()
        return redirect('rooms:room', room_name)


def room(request, room_name):
    group = StudentGroup.objects.get(pk=room_name)
    tasks = group.activity.tasks.all()
    students = group.students.filter(online=True)
    answers = group.answer_set.all()
    if request.user.is_authenticated:
        user = request.user
    else:
        uid = request.session['uid']
        user = Student.objects.get(pk=uid)

    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        f'room_{room_name}', {
            'type': 'users_list',
        }
    )
    return render(request, 'room.html', {
        'group': group,
        'tasks': tasks,
        'answers': answers,
        'students': students,
        'user': user
    })
