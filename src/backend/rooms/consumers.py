from django.template.loader import render_to_string
from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
import json
from .models import Student, StudentGroup


class GroupRoom(WebsocketConsumer):

    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_uid']
        self.room_group_name = f'room_{self.room_name}'
        uid = self.scope['session']['uid']
        user = Student.objects.get(pk=uid)
        user.set_online(True)
        user.save()
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )

        self.accept()

        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': 'users_list'
            }
        )

    def disconnect(self, close_code):
        uid = self.scope['session']['uid']
        user = Student.objects.get(pk=uid)
        user.set_online(False)
        user.save()
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': 'users_list'
            }
        )
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    def receive(self, text_data):
        data = json.loads(text_data)
        if 'user_ready' in data.keys():
            user_ready = data['user_ready']
            async_to_sync(self.channel_layer.group_send)(
                self.room_group_name,
                {
                    'type': 'users_list',
                    'user_ready': user_ready
                }
            )

            group = StudentGroup.objects.get(pk=self.room_name)
            group_students = group.students.filter(online=True)
            ready_students = group_students.filter(online=True, ready=True)

            if group_students.count() == ready_students.count():
                group = group.set_group_start()
                async_to_sync(self.channel_layer.group_send)(
                    self.room_group_name,
                    {
                        'type': 'tasks_list',
                        'tasks': 'ready_for_task'
                    }
                )

    def users_list(self, event):
        if 'user_ready' in event.keys():
            user = Student.objects.get(pk=event['user_ready'])
            user.set_ready(True)

        users = Student.objects.filter(online=True, group_id=self.room_name)
        context = {'students': users}
        html = render_to_string('inc-room-user-list.html', context)
        self.send(text_data=json.dumps({
            'event': 'Send',
            'users_list': html
        }))

    def tasks_list(self, event):
        group = StudentGroup.objects.get(pk=self.room_name)
        tasks = group.activity.tasks.all()
        answers = group.answer_set.all()
        context = {'tasks': tasks, 'group': group, 'answers': answers}
        html = render_to_string('inc-tasks-list.html', context)
        self.send(text_data=json.dumps({
            'event': 'Send',
            'tasks': html
        }))
