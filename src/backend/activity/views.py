import json
from django.shortcuts import render, HttpResponse
from django.template.loader import render_to_string
from django.core.exceptions import ObjectDoesNotExist
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
from .models import Activity, Exercise, Answer
from .forms import AnswerForm


# Create your views here.


def index(request):
    answers = Answer.objects.filter(status=Answer.CHECK)
    activities = Activity.objects.all()

    return render(request, 'dashboard.html', {'activities': activities, 'answers': answers})


def task_view(request, task_id):
    task = Exercise.objects.get(pk=task_id)
    group = request.META['HTTP_REFERER'].split('/')[-2]
    try:
        answer = Answer.objects.filter(task=task, group_id=group).last()
    except ObjectDoesNotExist:
        answer = None

    if request.method == 'GET':
        if not answer or answer.status == Answer.REJECTED:
            form = AnswerForm(task=task)
            context = {'form': form, 'task': task}
            return_str = render_to_string('inc-answer-form.html', context, request=request)
            return HttpResponse(json.dumps(return_str), content_type='application/json')
        elif answer and answer.status == Answer.DONE:
            return_str = render_to_string('inc-answer-done.html')
            return HttpResponse(json.dumps(return_str), content_type='application/json')
        elif answer and answer.status == Answer.CHECK:
            return_str = render_to_string('inc-answer-incheck.html')
            return HttpResponse(json.dumps(return_str), content_type='application/json')

    if request.method == 'POST':
        form = AnswerForm(request.POST, request.FILES)
        if form.is_valid():
            answer = form.save(commit=False)
            answer.task = task
            answer.group_id = group
            answer.save()
        else:
            context = {'form': form, 'task': task}
            return_str = render_to_string('inc-answer-form.html', context, request=request)
            return HttpResponse(json.dumps(return_str), content_type='application/json')
        return_str = render_to_string('inc-answer-incheck.html')
        return HttpResponse(json.dumps(return_str), content_type='application/json')


def get_answer(request, answer_pk):
    answer = Answer.objects.get(pk=answer_pk)
    if request.method == 'GET':
        return_str = render_to_string('inc-answer-check.html', {'answer': answer}, request=request)
        return HttpResponse(json.dumps(return_str), content_type='application/json')

    if request.method == 'POST':
        status = int(request.POST.get('status'))
        if status == 1:
            answer.status = answer.DONE
        elif status == 0:
            answer.status = answer.REJECTED
        answer.save(update_fields=['status'])

        room_name = answer.group_id
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            f'room_{room_name}', {
                'type': 'tasks_list',
                'event': 'task_checked',
            }
        )
        answers = Answer.objects.filter(status=Answer.CHECK)
        return_str = render_to_string('inc-answers-admin.html', {'answers': answers})
        return HttpResponse(json.dumps(return_str), content_type='application/json')
