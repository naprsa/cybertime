import json
from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from django.shortcuts import HttpResponse
from django.template.loader import render_to_string
from django.utils.html import mark_safe
from .models import Answer


class DashboardConsumer(WebsocketConsumer):

    def connect(self):
        self.room_name = 'dashboard'

        async_to_sync(self.channel_layer.group_add)(
            self.room_name,
            self.channel_name
        )
        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(
            self.room_name,
            self.channel_name
        )

    def receive(self, text_data):
        data = json.loads(text_data)
        if 'answer_check' in data.keys():
            answer_pk = data['answer_check']
            async_to_sync(self.channel_layer.group_send)(
                self.room_name,
                {
                    'type': 'answer_check',
                    'answer_pk': answer_pk
                }
            )

    def answer_check(self, event):
        answers = Answer.objects.filter(status=Answer.CHECK)
        context = {'answers': answers}
        return_str = render_to_string('inc-answers-admin.html', context)
        self.send(text_data=json.dumps({
            'event': 'Send',
            'answers_for_check': return_str
        }))
