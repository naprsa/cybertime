from django import forms
from .models import Answer


class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        fields = ['file', 'text', 'url', 'num']
        widgets = {
            'text': forms.Textarea()
        }

    def __init__(self, *args, task=None, **kwargs):
        super(AnswerForm, self).__init__(*args, **kwargs)
        if task:
            if not task.can_file:
                self.fields.pop('file')
            if not task.can_text:
                self.fields.pop('text')
            if not task.can_url:
                self.fields.pop('url')
            if not task.can_num:
                self.fields.pop('num')
