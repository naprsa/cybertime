from django.urls import path

from . import views

app_name = 'activity'

urlpatterns = [
    path('', views.index, name='dashboard'),
    path('task/<int:task_id>/', views.task_view, name='task'),
    path('answer/<int:answer_pk>', views.get_answer, name='answer'),

]
