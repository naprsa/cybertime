from django.shortcuts import reverse
from django.db import models
from django.db.models.signals import post_save
from shortuuidfield import ShortUUIDField
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync


class ExerciseCategory(models.Model):
    title = models.CharField(verbose_name='Название', max_length=200)

    class Meta:
        ordering = ['title']
        verbose_name = 'Категория задания'
        verbose_name_plural = 'Категории заданий'

    def __str__(self):
        return self.title


class Exercise(models.Model):
    LEVELS = (
        (1, 'Уровень 1'),
        (2, 'Уровень 2'),
        (3, 'Уровень 3'),
        (4, 'Уровень 4'),
        (5, 'Уровень 5')
    )

    category = models.ForeignKey(ExerciseCategory, verbose_name='Категория', related_name='tasks',
                                 on_delete=models.SET_NULL, null=True)
    level = models.PositiveSmallIntegerField(verbose_name='Уровень', choices=LEVELS, default=1)
    text = models.TextField(verbose_name='Текст задания')
    exercise_link = models.URLField(verbose_name='Доп. ссылка', blank=True)
    exercise_file = models.FileField(verbose_name='Доп. файл', blank=True, null=True, upload_to='activity/files')
    exercise_image = models.ImageField(verbose_name='Доп. изображение', blank=True, null=True,
                                       upload_to='activity/images')
    points = models.IntegerField(verbose_name='Баллы', default=0)
    delta_points = models.DecimalField(verbose_name='Дельта баллы', max_digits=6, decimal_places=3, default=0)
    can_file = models.BooleanField(verbose_name='Файл в ответе', default=False)
    can_url = models.BooleanField(verbose_name='Ссылка в ответе', default=False)
    can_text = models.BooleanField(verbose_name='Текст в ответе', default=False)
    can_num = models.BooleanField(verbose_name='Число в ответе', default=False)
    auto_answer = models.TextField(verbose_name='Авто ответ', blank=True)

    class Meta:
        ordering = ['category', 'level']
        verbose_name = 'Задание'
        verbose_name_plural = 'Задания'

    def __str__(self):
        return f'{self.category} - уровень {self.level}'


class Answer(models.Model):
    DONE = 'done'
    CHECK = 'check'
    REJECTED = 'rejected'

    CHOICES = (
        (DONE, 'Выполнено'),
        (CHECK, 'Проверяется'),
        (REJECTED, 'Отклонено')
    )

    status = models.CharField(verbose_name='Статус', max_length=10, choices=CHOICES, default=CHECK)
    group = models.ForeignKey('rooms.StudentGroup', verbose_name='Группа', on_delete=models.SET_NULL, null=True)
    task = models.ForeignKey(Exercise, verbose_name='Задание', on_delete=models.SET_NULL, null=True,
                             related_name='answers')
    text = models.TextField(verbose_name='Текст ответа', max_length=500, blank=True, null=True)
    file = models.FileField(verbose_name='Прикрепленный файл', upload_to='answers/', blank=True, null=True)
    url = models.URLField(verbose_name='Ссылка', blank=True, null=True)
    num = models.DecimalField(verbose_name='Числовой ответ', max_digits=10, decimal_places=6, blank=True, null=True,
                              default=0)
    created = models.DateTimeField(verbose_name='Создано', auto_now_add=True, editable=False)

    class Meta:
        ordering = ['created', 'status', 'task']

    def __str__(self):
        return f'{self.task} - {self.status}'

    def get_absolute_url(self):
        return reverse('activity:answer', kwargs={'answer_pk': self.pk})


def post_save_answer(sender, instance, created, **kwargs):
    if created:
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            'dashboard', {
                'type': 'answer_check',
                'event': 'answer_pk',
                'answer_pk': instance.pk
            }
        )


post_save.connect(post_save_answer, Answer)


class Activity(models.Model):
    uid = ShortUUIDField(primary_key=True, editable=False, max_length=10, verbose_name='Номер активности')
    name = models.CharField(verbose_name='Название', max_length=200)
    tasks = models.ManyToManyField(Exercise, verbose_name='Задания', blank=True)
    groups = models.IntegerField(verbose_name='Кол-во групп', default=1)

    def __str__(self):
        return self.name


def post_save_activity(sender, instance, created, **kwargs):
    if created:
        num_groups = instance.groups
        for _ in range(num_groups):
            instance.students_groups.create(activity=instance)


post_save.connect(post_save_activity, Activity)
