from django.contrib import admin

from .models import Exercise, ExerciseCategory, Activity, Answer

admin.site.register(Exercise)
admin.site.register(ExerciseCategory)
admin.site.register(Activity)
admin.site.register(Answer)
