from django.urls import re_path, path
from channels.routing import ProtocolTypeRouter, URLRouter, ChannelNameRouter
from channels.auth import AuthMiddlewareStack
# from channels.staticfiles import St

from rooms import consumers as rooms_consumers
from activity import consumers as activity_consumers

websocket_urlpatterns = [
    re_path(r'rooms/room/(?P<room_uid>\w+)', rooms_consumers.GroupRoom),
    path('', activity_consumers.DashboardConsumer)
]

application = ProtocolTypeRouter({
    'websocket': AuthMiddlewareStack(
        URLRouter(websocket_urlpatterns)
    ),
    # 'http.request':
})
