from .base import *

DEBUG = False

SECRET_KEY = '90l(6lvi&c*z(n1tli+e=2!gjz70n-yb%el@=-)_u7q3@_@&pf'

ALLOWED_HOSTS = ['45.12.18.210', ]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'ct',
        'USER': 'cybertime',
        'PASSWORD': 'cybertimepasswordpostgres',
        'HOST': 'localhost',
        'PORT': '',  # Set to empty string for default.
    }
}
