from .base import *

try:
    from .prod import *
except ImportError:
    print("Can't import config... exit")
    exit()

try:
    from .dev import *
except ImportError:
    pass
